const game = document.querySelector(".game");
const span = document.querySelector("span");
const controlText = document.querySelector("h2");
const number = getRandomNumber();
const body = document.querySelector("body");


window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
const recognition = new window.SpeechRecognition();
recognition.start()

function onSpeak(e) {
    const result = e.results[0][0].transcript;
    console.log(result)
    game.style.display = "flex";
    span.innerText = result;
    if(!isNaN(result)){
        if(result <= 100 && result >= 1) {
            checkNumber(+result);
        }else {
            controlText.innerText = "Number must be 1-100";
        }
    } else {
        controlText.innerText = "It's not a number";
    }
}

recognition.addEventListener('result', onSpeak);
recognition.addEventListener('end', () => {
    setTimeout(() => {
        recognition.start()
    },300)
})

function getRandomNumber() {
    return Math.floor(Math.random() * 100);
}

function checkNumber(result) {
    if(number === result){
        winGame(number);
    } else if(result > number) {
        controlText.innerText = "Go Lower";
    } else if(result < number) {
        controlText.innerText = "Go Higher"
    }
}

function winGame(number) {
    body.innerHTML = 
    `<h2>Congrats You have guessed number!</h2>
    <h3>It was ${number}</h3>
    <button class="playBtn">Play again</button>`;
    const playBtn = document.querySelector(".playBtn");
    playBtn.addEventListener("click",() => window.location.reload());
}


